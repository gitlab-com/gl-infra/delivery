### Context
It is time to start planning the upcoming release management cover for the `[list of releases]` releases. For reference, you can see the [previous schedule](https://about.gitlab.com/community/release-managers/).

### Release Managers TODO
**Please put your name down for the milestones you would be available to cover. Please note the new release dates for these quarters**. Writing your name down for a milestone is only an indication of your availability, it will not result in your being assigned to cover every milestone.

**Due Date: `[due date]`**

On `[schedule creation date]`, we will work through all the options and create an MR with a sustainable schedule for you all to approve. We will do our best to avoid having RM rotations with all the team members belonging to the same team to give each team enough time to carry on project work.

| Milestone | Dates | EMEA Availability | AMER Availability |
|-----------|-------|-------------------|-------------------|
| XX.X | start date - end date | `@user1`, `@user2`  | `@user3`, `@user4` |
| XX.X | start date - end date | `@user1`, `@user2`  | `@user3`, `@user4` |
| XX.X | start date - end date | `@user1`, `@user2`  | `@user3`, `@user4` |

### Managers TODO

- [ ] Reflect Rotations via a MR on: https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/releases.yml?ref_type=heads
- [ ] Reflect Rotation on [release-managers](https://gitlab.pagerduty.com/schedules#PY12KEX) PagerDuty Schedule

/label ~Delivery::P2 ~group::delivery ~workflow-infra::Triage ~Discussion
