## Intent
This issue describes how broader group and team members are [facilitating a team member's role to take meaningful time away from work](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off). The intent is that the team member isn't required to "catchup" before and after taking a well-deserved vacation. 

## :handshake: Responsibilities
Include creating release post items, weekly triage issues, responding to community or customer requests, preparing a Kickoff video, prioritizing issues, responding to clarification in current milestone work, etc.

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
| HIGH     |       |              |         |           |
| MEDIUM   |       |              |         |           |

## :muscle: Coverage Tasks
Include specific issue links for tasks to be completed. Team meeting, planning, kickoff, or GC prep, etc.
- [ ] Item - Link - `@assignee`

## :book: References

## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `PTO Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Assign due date for your last PTO day
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via [Time Off by Deel](https://about.gitlab.com/handbook/paid-time-off/#time-off-by-deel) including assigning some tasks to a relevant #g_ , #s_ or #product slack channel
- [ ] Ensure your Time Off by Deel auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue

### :x: Closing Tasks
- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/paid-time-off/#returning-from-pto) guidance
- [ ] **PM only**: Review the [PM Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/gl-infra/delivery/-/tree/main/.gitlab/issue_templates/pto_coverage.md)

