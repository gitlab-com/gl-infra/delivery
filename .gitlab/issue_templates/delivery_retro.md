Hello all, FY<year>Q<1,2,3,4> is over and it is time for us to reflect on the quarter and see what we want to bring with us into FY<year>Q<1,2,3,4> and what we want to improve.

### Q<number> goals

We went into FY<year>Q<1,2,3,4> with these OKRs:

1.
2.

### Improvements areas from prior quarter Retro



## Retro time

Please add a comment with your thoughts on the quarter and suggestions for an (even) better next quarter:

1. How was FY<year>Q<1,2,3,4> for you?
2. What went well on the project you worked on? What went not so well? What could have made you and the project more successful?
3. Did you see improvements from the FY<year>Q<1,2,3,4> Improvements areas?
4. What one (or more!) area of the Delivery Group would you like to see improve?
5. What thanks/call outs do you have for anyone?

`@gitlab-org/delivery` please take some time to reflect on our last quarter and share your thoughts.
/label ~Discussion ~group::delivery ~Delivery::P4 ~workflow-infra::Triage