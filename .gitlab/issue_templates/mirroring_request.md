# Project informations

- Canonical repository path: {+TODO+}
- Project maintainers: {+TODO+}

## Tasks

Before engaging with the team, please follow these tasks:

- [ ] set a title to `Mirroring request for <PROJECT_NAME>`
- [ ] compile the project informations section
- [ ] grant `@gitlab-release-tools-bot` maintainer access to the project
- [ ] grant `@gitlab-org/delivery` group maintainer access to the project
- [ ] grant `@gitlab-org/release/managers` maintainer access to the project
- [ ] apply the ~"workflow-infra::Triage" and ~"Delivery team::Build" labels

## Delivery tasks

- [ ] follow our [mirroring documentation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/mirrors.md)

/label ~"group::delivery" ~"Delivery::P4" ~"workflow-infra::Proposal"
